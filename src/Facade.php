<?php

namespace CarbonApp;

// Extend the base Facade class to provide a public method to get the accessor string so it only has to be specified in the Facade (as opposed to both in the Facade and Container)
// All classes that extend this one should define the getAppAccessor() method instead of the getFacadeAccessor()
class Facade extends \Illuminate\Support\Facades\Facade {
	public static function getAppAccessor() {
		throw new \RuntimeException('Facade does not implement getAppAccessor method.');
	}

	protected static function getFacadeAccessor() {
		return static::getAppAccessor();
	}
}