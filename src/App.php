<?php

namespace CarbonApp;

use \Pimple\Container;

class App {
	public $container;

	public function __construct() {
		$this->container = new Container();
		Facade::setFacadeApplication($this->container);
		AliasLoader::getInstance()->register();
	}

	public function register($facade_class, $alias, $value) {
		$key = $facade_class::getAppAccessor();
		$this->container[$key] = $value;
		AliasLoader::getInstance()->alias($alias, $facade_class);
	}
}